<?php
require_once('vendor/autoload.php');
require_once('bootstrap/config.php');

use BStupar\Helper as H;

//get instance of Twitter lib class
$tw = new \BStupar\Twitter($config);

// default page parameters
$page = 1;
$display = 10;

// number of tweets to display per page
if (isset($_GET['display'])) {
    if (is_numeric($_GET['display'])) {
        $display = $_GET['display'];
    }
}
//check if we have page request or not
if (isset($_GET['page'])) {
    if (is_numeric($_GET['page'])) {
        $page = $_GET['page'];
    }
}

//get tweets
$tweets = $tw->getTweets($display, $page);
$max_pages = $tw->numPages($display);
$colors = $tw->getColors();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>IMW Twitter test app</title>
    <link rel="stylesheet" href="css/style.css">
    <style>
        blockquote.twitter-tweet p {
            color: #<?php echo H::rmQuote($colors->text_color); ?>;
        }
        blockquote.twitter-tweet a {
            color: #<?php echo H::rmQuote($colors->link_color); ?>;
        }
    </style>
</head>
<body>
<div class="tweet-container">
    <?php foreach($tweets as $tweet) { ?>
        <blockquote class="twitter-tweet">
            <div class="Tweet-header">
                <div class="TweetAuthor">
                    <p align="left">
                        <span class="handle-name"> <?php echo H::rmQuote($tweet['handle']); ?></span> <a class="handle" href="https://twitter.com/<?php echo H::rmQuote($tweet['handle_name']); ?>">
                            @<?php echo H::rmQuote($tweet['handle_name']);?>
                        </a>
                        · <a href="https://twitter.com/b92vesti/status/<?php echo $tweet['tweet_id']; ?>">
                            <?php echo H::diffDates($tweet['created_at']); ?>
                        </a>
                    </p>
                    <span class="TweetAuthor-avatar">
                        <img class="Avatar" src="<?php echo H::rmQuote($tweet['handle_avatar']); ?>" alt="">
                    </span>
                </div>
            </div>

            <p><?php echo H::addLinks($tweet['tweet']); ?></p>
            <a href="https://twitter.com/b92vesti/status/<?php echo $tweet['tweet_id']; ?>">
                <img src="<?php echo $tweet['image_link']; ?>" alt="" />
            </a>
        </blockquote>
    <?php } ?>

    <div class="pagination">
        <?php if ($page == 1) {?>
            &laquo; &lsaquo;
            Strana <?php echo $page.' od '. $max_pages; ?>
            <a href="index.php?page=<?php echo ($page+1) .'&display='.$display?>">&rsaquo;</a> <a href="index.php?page=<?php echo $max_pages.'&display='.$display?>">&raquo;</a>
        <?php } else { ?>
            <a href="index.php?page=1&display=<?php echo $display?>">&laquo;</a> <a href="index.php?page=<?php echo ($page-1) .'&display='.$display?>">&lsaquo;</a>
            Strana <?php echo $page.' od '. $max_pages; ?>
            <a href="index.php?page=<?php echo ($page+1) .'&display='.$display?>">&rsaquo;</a> <a href="index.php?page=<?php echo $max_pages.'&display='.$display?>">&raquo;</a>
        <?php } ?>
    </div>
    </div>

</body>
</html>
