<?php
namespace BStupar;

use BStupar\Curl as Curl;
use PDO;

class Twitter {

  // auth token for requests
  private $bearer;
  private $con;

  /**
   * Constructor method
   * @param array $config    config array
   */
  public function __construct($config)
  {
    // api url for request
    $url = 'https://api.twitter.com/oauth2/token';

    // set curl options for request to twitter api
    $options = [
      CURLOPT_USERAGENT       => 'IMW Twitter Test Agent',
      CURLOPT_HTTPHEADER      => [
        'Authorization: Basic ' . base64_encode($config['consumer_key'] .':'. $config['consumer_secret']),
        'Content-Type: application/x-www-form-urlencoded;charset=UTF-8'
      ],
      CURLOPT_HEADER          => false,
      CURLOPT_POST            => true,
      CURLOPT_POSTFIELDS      => 'grant_type=client_credentials',
      // include SSL
      CURLOPT_SSL_VERIFYPEER  => true,
      CURLOPT_SSL_VERIFYHOST  => 2,
      CURLOPT_VERBOSE         => false,
      CURLOPT_CAINFO          => __DIR__ . '/../cert/apitwittercom.cer',
      CURLOPT_RETURNTRANSFER  => true
    ];

    // pass the value for curl request and process response
    $ch = Curl::get($url, $options);
    // response is json object
    $result = json_decode($ch);
    // save bearer since we need it to make api calls
    if (isset($result->token_type) && $result->token_type === "bearer") {
        $this->bearer = $result->access_token;
    }

    // we need database connection so ..
    try {
      $this->con = new PDO("mysql:host={$config['host']};dbname={$config['dbname']};charset=utf8", $config['user'], $config['pass'], array(
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION));

    } catch (PDOException $e) {
      echo "Oops!An error occured!";
    }
  }

  /**
   * Get tweets for specified user
   * @param  string  $user     twitter handle - username
   * @param  integer $since_id last tweet's ID
   * @return json              json object of new posts
   */
  public function getApiTweets($user, $since_id = false)
  {
    // get parameters
    // specified by twitter API
    $parameters = [
      'screen_name' => $user,
      'include_rts' => false,
    ];

    // if we don't have since_id set that means we are doing first pull to the database
    // if ($since_id === false) {
    //   $parameters['count'] = 5;
    // }
    if ($since_id) {
      $parameters['since_id'] = $since_id;
    }

    $api_url = 'https://api.twitter.com/1.1/statuses/user_timeline.json?'. http_build_query($parameters);

    // set curl options for request to twitter api
    $options = [
      CURLOPT_USERAGENT      => 'IMW Twitter Test Agent',
      CURLOPT_HTTPHEADER     => [
        'Authorization: Bearer ' . $this->bearer
      ],
      CURLOPT_HEADER         => false,
      CURLOPT_POST           => false,
      // include SSL
      CURLOPT_SSL_VERIFYPEER => true,
      CURLOPT_SSL_VERIFYHOST => 2,
      CURLOPT_VERBOSE        => false,
      CURLOPT_CAINFO         => __DIR__ . '/../cert/apitwittercom.cer',
      CURLOPT_RETURNTRANSFER => true
    ];

    $ch = Curl::get($api_url, $options);
    $tweets = json_decode($ch, true);
    $this->saveTweets($tweets);
  }

  /**
   * Save tweets in to the database
   * @param  array  $tweets    tweets in json format
   * @return void              json object of new posts
   */
  private function saveTweets($tweets)
  {
    // prepare for insert
    $insert = $this->con->prepare("INSERT INTO Tweets (tweet_id, tweet, created_at, favorited, retweeted, raw) VALUES (?, ?, ?, ?, ?, ?)");
    $insert->bindParam(1, $tweet_id);
    $insert->bindParam(2, $tweet);
    $insert->bindParam(3, $created_at);
    $insert->bindParam(4, $favorited);
    $insert->bindParam(5, $retweeted);
    $insert->bindParam(6, $raw);

    // iterate over tweets and insert them
    // we do reverse iteration from higher to lower since $item[0]
    // is newest tweet and needs to be stored last not first !!!
    for ($i = count($tweets) - 1; $i >= 0; $i--) {
      $tw         = $tweets[$i];
      $tweet_id   = $tw['id'];
      $tweet      = $tw['text'];
      $created_at = $tw['created_at'];
      $favorited  = $tw['favorited'];
      $retweeted  = $tw['retweeted'];
      $raw        = json_encode($tw);
      $insert->execute();
      $this->checkForImages($tw);
    }
  }

  /**
   * check if there are images in the post
   * @param  array  $tweet     single tweet in array
   * @return void
   */
  private function checkForImages($tweet)
  {
    //check if media part is set and what type is it ?
    if (array_key_exists('media', $tweet['entities'])) {
      // if media type is photo (there are other media types tho ..)
      foreach($tweet['entities']['media'] as $media) {
        if ($media['type'] === 'photo') {
          // save in DB image link, since we will need it !
          $query = $this->con->prepare("INSERT INTO Media (tweet_id, image_link) VALUES (?, ?)");
          $query->bindParam(1, $tweet_id);
          $query->bindParam(2, $image_link);

          $tweet_id   = $tweet['id'];
          $image_link = $media['media_url'];
          $query->execute();
        }
      }

    }
  }

  /**
   * retrive $display number of tweets from the database and skip $page amount of them
   * @param  integer  $display    number of tweets to retreive from database
   * @param  integer  $page    number of tweets to skip from retrieval
   * @return array             array of tweets
   */
  public function getTweets($display = 10, $page = 0)
  {
    $page = ($page-1)*10;
    $sql = "SELECT
              tweet,
              created_at,
              favorited,
              retweeted,
              raw->'$.user.name' as handle,
              raw->'$.user.screen_name' as handle_name,
              raw->'$.user.id' as handle_id,
              raw->'$.user.profile_image_url' as handle_avatar,
              Media.*
              FROM Tweets
              LEFT JOIN Media on Tweets.tweet_id = Media.tweet_id
              ORDER BY id desc
              LIMIT {$display} OFFSET {$page}";

    $query = $this->con->query($sql);
    $result = [];
    while ($q = $query->fetch(PDO::FETCH_ASSOC)) {
      $result[] = $q;
    }
    return $result;
  }

  /**
   * Get last tweet's ID from database'
   * @return bool|object            return false or object if found
   */
  public function lastTweet()
  {
    $sql = "SELECT tweet_id FROM Tweets ORDER BY id desc LIMIT 1 OFFSET 0";
    $query = $this->con->query($sql);
    return $query->fetch(PDO::FETCH_OBJ);
  }

  /**
   * Return total number of tweets so that we can calculate pagination
   * @method numPages
   * @param  integer   $display number of tweets per page
   * @return integer             number of pages
   */
  public function numPages($display)
  {
    $sql = "SELECT COUNT(*) as total FROM Tweets";
    $query = $this->con->query($sql);
    $tweets = $query->fetch(PDO::FETCH_OBJ);
    return ceil($tweets->total / $display);
  }

  /**
   * Get css color codes for links and text
   * @method getColors
   * @return object    ->link_color and ->text_color
   */
  public function getColors()
  {
    $sql = "SELECT raw->'$.user.profile_link_color' as link_color, raw->'$.user.profile_text_color' as text_color FROM Tweets ORDER by id desc LIMIT 1";
    $query = $this->con->query($sql);
    return $query->fetch(PDO::FETCH_OBJ);
  }
}
