<?php
namespace BStupar;

class Curl {

  /**
   * Curl Get method
   * @param  string $url     URL for request
   * @param  array  $options CURL options
   * @return mixed           response
   */
  public static function get($url, array $options)
  {
    $ch = curl_init();

    if (isset($options) && is_array($options)) {
      curl_setopt_array($ch, $options);
    }

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
  }

}
