<?php
namespace BStupar;

class Helper {


  /**
   * Calculate difference between current date and tweets post date
   * @param   string  $date   tweets string date format
   * @return  string
   */
  public static function diffDates($date)
  {
    $date_format = 'D M d H:i:s O Y';
    $tweet_time = date_create_from_format($date_format, $date);
    // convert to microtime
    $tweet_microtime = date_format($tweet_time, 'U');
    $current_microtime = date('U');
    //compare years
    if (((int) date('Y', $current_microtime) - (int) date('Y', $tweet_microtime)) != 0) echo date('M d', $tweet_microtime);
    // compare months
    elseif (((int) date('n', $current_microtime) - (int) date('n', $tweet_microtime)) != 0) echo date('M d', $tweet_microtime);
    // compare days
    elseif (((int) date('j', $current_microtime) - (int) date('j', $tweet_microtime)) != 0) echo date('M d', $tweet_microtime);
    // compare hours
    else {
      // convert to hours to minutes and add them together
      $current_minutes = (int) date('H', $current_microtime) * 60 + (int) date('i', $current_microtime);
      $tweet_minutes = (int) date('H', $tweet_microtime) * 60 + (int) date('i', $tweet_microtime);
      $diff_minutes = $current_minutes - $tweet_minutes;
      switch ($diff_minutes) {
        case ($diff_minutes > 60):
          echo (int) date('H', $current_microtime) - (int) date('H', $tweet_microtime).'h';
          break;
        case ($diff_minutes > 0):
          echo ceil($diff_minutes).'m';
          break;
        case ($diff_minutes == 0):
          echo "Now";
      }
    }
  }


  /**
   * 'Fancy' way to remove string from index page
   * built only for readability of code
   * @param   string  $string   string to remove quotes from
   * @return  string
   */
  public static function rmQuote($string)
  {
    return str_replace('"', '',$string);
  }

  /**
   * Add links to the tweet
   * @param   string  $string  tweet string
   * @return  string
   */
  public static function addLinks($string)
  {
    return preg_replace('/((www|https:\/\/|http:\/\/)[^ ]+)/', '<a href="\1">\1</a>', $string);
  }
}
