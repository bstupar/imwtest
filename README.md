## IMW Test Twitter API integration project
**index.php** - Entry point and displays tweets. Accepts two parameters

1.  **_page_** - number of page to display
2.  **_display_** - number of pages to be displayed per page

**cron.php** - For fetching new tweets

> Before usage import database and change config file by your needs from **_bootstrap/config.php_** file