<?php
require_once('vendor/autoload.php');
require_once('bootstrap/config.php');

//get instance of Twitter lib class
$tw = new \BStupar\Twitter($config);

$last_tw_id = $tw->lastTweet();

// if we have last tweet's id we will use it for fetching new ones
// if not, pull last 200 tweets
if ($last_tw_id) {
    $tw->getApiTweets($config['handle'], $last_tw_id->tweet_id);
} else {
    $tw->getApiTweets($config['handle']);
}
